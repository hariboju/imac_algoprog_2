#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
    for (int i=0; i<nodeCount; i++){
            GraphNode* newNode = new GraphNode(i);
            this->appendNewNode(newNode);
        }
         for (int i=0; i<nodeCount; i++){
           for (int h=0; h<nodeCount; h++){
               if (adjacencies[i][h] != 0){
                   this->nodes[i]->appendNewEdge(this->nodes[h], adjacencies[i][h]);
               }
           }
        }



}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
    nodes[nodesSize] = first;
         visited[first->value] = true;
         nodesSize++;

         for (Edge* a = first->edges; a != nullptr; a = a->next){
             if (visited[a->destination->value] == false){
                deepTravel(a->destination, nodes, nodesSize, visited);
             }
         }


}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);

    while (nodeQueue.size()!=0){
            GraphNode* node = nodeQueue.front();
            nodeQueue.pop();
            nodes[nodesSize] = node;
            nodesSize++;
            visited[node->value] = true;

            for (Edge* a = node->edges; a != nullptr; a= a->next){
                if (visited[a->destination->value] == false){
                    nodeQueue.push(a->destination);
                }
            }
    }


}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{

    visited[first->value ]= true;

        for (Edge* a = first->edges; a!=NULL; a=a->next){
            if (!visited[a->destination->value])
                detectCycle(a->destination, visited);
            else
                return true;
        }

    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
