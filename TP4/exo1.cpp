#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex){
    return nodeIndex * 2 + 1;
}

int Heap::rightChild(int nodeIndex){
    return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value){
    int a = heapSize;
    this->set(a, value);
    while(a>0 && this->get(a) > this->get((a-1)/2)){
        swap(a, (a-1)/2);
        a = (a-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex){
    int i_max = nodeIndex;

    if(leftChild(nodeIndex) < heapSize && this->get(leftChild(nodeIndex)) > this->get(i_max)){
        i_max = leftChild(nodeIndex);
    }

    if(rightChild(nodeIndex) < heapSize && this->get(rightChild(nodeIndex)) > this->get(i_max)){
        i_max = rightChild(nodeIndex);
    }
    if(i_max != nodeIndex){
        swap(nodeIndex, i_max);
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers){
    for(int i=0; i<numbers.size(); i++){
        this->insertHeapNode(i, numbers.get(i));
    }
}

void Heap::heapSort(){
    for(int i=this->size()-1; i>=0;i--){
        swap(0, i);
        heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
