#include "tp1.h"
#include <QApplication>
#include <time.h>

void compCar(Point * comp){
    comp->x = pow(comp->x, 2) - pow(comp->y, 2);
    comp->y = 2*comp->x*comp->y;
}


int isMandelbrot(Point z, int n, Point point){

    if(n >= 0){
           compCar(&z);
           z.x += point.x;
           z.y += point.y;

           if(sqrt(pow(z.x,2) + pow(z.y,2)) > 2){
               return 1;
           }else{
               return isMandelbrot(z,n-1,point);
           }
       }else{
           return 0;
       }
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



